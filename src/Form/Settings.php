<?php

namespace Drupal\webform_location_addressfinder\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Webform Location Addressfinder.
 */
class Settings extends ConfigFormBase {

  /**
   * Bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_location_addressfinder.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_location_addressfinder_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_location_addressfinder.settings');

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('The API key to use for AddressFinder'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key') ?? ''
    );

    $form['no_postal'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('No postal addresses?'),
      '#default_value' => $config->get('no_postal') ?? FALSE,
    );

    $form['is_nz'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Addresses for NZ instead of Australia?'),
      '#default_value' => $config->get('is_nz') ?? FALSE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('webform_location_addressfinder.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('no_postal', $form_state->getValue('no_postal'))
      ->set('is_nz', $form_state->getValue('is_nz'))
      ->save();

  }

}
