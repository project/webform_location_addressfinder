<?php

namespace Drupal\webform_location_addressfinder\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformLocationBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an 'location' element using AddressFinder.com.au.
 *
 * @WebformElement(
 *   id = "webform_location_addressfinder",
 *   label = @Translation("Location (AddressFinder)"),
 *   description = @Translation("Provides a form element to collect valid location information (address, etc) using addressfinder.com.au."),
 *   category = @Translation("Composite elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class WebformLocationAddressFinder extends WebformLocationBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
      'override_defaults' => FALSE,
      'api_key' => '',
      'no_postal' => FALSE,
      'is_nz' => FALSE,
      'placeholder' => '',
    ] + parent::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLabel() {
    return $this->elementManager->isExcluded('webform_location_addressfinder') ? $this->t('Location') : parent::getPluginLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['composite']['override_defaults'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override Address Finder default settings'),
    ];

    $form['composite']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AddressFinder API key'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' =>
            TRUE]],
        ],
      ],
    ];

    $form['composite']['is_nz'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Get addresses for New Zealand instead of Australia'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $form['composite']['no_postal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable use of postal addresses'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return $form;
  }

}
