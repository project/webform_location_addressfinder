<?php

namespace Drupal\webform_location_addressfinder\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\TextField;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides an 'location' element using AddressFinder.com.au.
 *
 * @WebformElement(
 *   id = "webform_location_addressfinder_fulladdressonly",
 *   label = @Translation("Location (AddressFinder) - Full Address Only"),
 *   description = @Translation("Provides a form element to collect a valid
 *   address using addressfinder.com.au."), category = @Translation("Advanced
 *   elements"),
 * )
 */
class WebformLocationAddressFinderFullAddressOnly extends TextField {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
        'override_defaults' => FALSE,
        'api_key' => '',
        'no_postal' => FALSE,
        'is_nz' => FALSE,
        'placeholder' => '',
      ] + parent::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLabel() {
    return $this->elementManager->isExcluded('webform_location_addressfinder_fulladdressonly') ? $this->t('Location') : parent::getPluginLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['override_defaults'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override Address Finder default settings'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AddressFinder API key'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' =>
            TRUE]],
        ],
      ],
    ];

    $form['is_nz'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Get addresses for New Zealand instead of Australia'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $form['no_postal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable use of postal addresses'),
      '#states' => [
        'visible' => [
          [':input[name="properties[override_defaults]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    if (empty($element['#webform_key'])) {
      return;
    }

    $settings = \Drupal::config('webform_location_addressfinder.settings');
    $field = $element['#webform_key'];

    // There can only be one API key.
    if (empty($element['#attached']['drupalSettings']['webform']['location']['addressfinder']['api_key'])) {
      $element['#attached']['drupalSettings']['webform']['location']['addressfinder']['api_key'] =
        empty($element['#override_defaults']) ? $settings->get('api_key') :
          ($element['#api_key'] ?? '') ;
    }

    $element['#attached']['drupalSettings']['webform']['location']['addressfinder'][$field] =
      empty($element['#override_defaults']) ?
        [
          'no_postal' => $settings->get('no_postal'),
          'is_nz' => $settings->get('is_nz'),
        ] : [
        'no_postal' => $element['#no_postal'],
        'is_nz' => $element['#is_nz'],
      ];

    $element['#attached']['library'][] = 'webform_location_addressfinder/webform.element.location.addressfinder';
  }

}
